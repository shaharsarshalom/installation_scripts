#!/bin/bash

# run everything as super user 
# sudo -i 

# based on ros installation guide 
# https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html

## Make sure you have a locale which supports
locale  # check for UTF-8

sudo apt update && sudo apt install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8

locale  # verify settings

## You will need to add the ROS 2 apt repository to your system.
sudo apt install software-properties-common -y
sudo add-apt-repository universe -y

# Now add the ROS 2 GPG key with apt.
sudo apt update && sudo apt install curl -y
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

# Then add the repository to your sources list.
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null


## Install ROS 2 packages
sudo apt update
sudo apt upgrade

# Desktop Install (Recommended): ROS, RViz, demos, tutorials.
sudo apt install ros-humble-desktop -y 

# Development tools: Compilers and other tools to build ROS packages
sudo apt install ros-dev-tools -y

# source the workspace 
# Define the line to add
# source /opt/ros/humble/setup.zsh
line_to_add="source /opt/ros/humble/setup.zsh"

# Check if .zshrc exists
if [ -f ~/.zshrc ]; then
    # Append the line to .zshrc
    echo "$line_to_add" >> ~/.zshrc
    echo "Line added to ~/.zshrc"
else
    echo "Error: ~/.zshrc file not found."
fi

####################
install RTQ plugins
rqt_command="ros-$ROS_DISTRO-rqt-*"
echo $rqt_command
sudo apt-get install $rqt_command

####################

####################
sudo snap install foxglove-studio
####################
## Install ros packages for development 

# used for colcon 
sudo apt install -y python3-colcon-common-extensions

sudo apt install -y python3-rosdep2 

sudo apt install -y build-essential

sudo apt install -y ros-$ROS_DISTRO-rosbridge-server

# install rqt 
sudo apt install -y ros-$ROS_DISTRO-rqt

# for velodyne ros package 
sudo apt install -y libpcap-dev

sudo apt install -y ros-$ROS_DISTRO-diagnostic-updater

sudo apt install -y ros-$ROS_DISTRO-pcl-ros

sudo apt install -y ros-$ROS_DISTRO-cv-bridge

sudo apt install -y ros-$ROS_DISTRO-stereo-msgs

sudo apt install -y ros-$ROS_DISTRO-sensor-msgs-py

sudo apt install -y ros-$ROS_DISTRO-pcl-conversions

# this doesnt work, not sure why? 
# sudo apt install -y ros-$ROS_DISTRO-nmea_msgs

sudo apt install -y ros-$ROS_DISTRO-common-interfaces
####################


#################### Camara calibration
# https://navigation.ros.org/tutorials/docs/camera_calibration.html
sudo apt install ros-$ROS_DISTRO-camera-calibration-parsers

sudo apt install ros-$ROS_DISTRO-camera-info-manager

sudo apt install ros-$ROS_DISTRO-launch-testing-ament-cmake
#################### Camara calibration

# for Camara calibration

# build ros2 vision_opencv  (which is cv_bridge) from source
cd ${ROS2_WS}
git clone https://github.com/ros-perception/vision_opencv.git
cd vision_opencv
git checkout -b $ROS_DISTRO
git status
cd ${ROS2_WS}
# the vision_opencv  contains 4 packages cv_bridge image_geometry opencv_tests vision_opencv
colcon build --packages-select cv_bridge image_geometry opencv_tests vision_opencv --symlink-install --allow-overriding image_geometry  vision_opencv --parallel-workers 4 --ament-cmake-args -DPYTHON_EXECUTABLE=/home/robo01/anaconda3/envs/CondaEnvForRos310/bin/python

#colcon build --packages-select vision_opencv --allow-overriding image_geometry  vision_opencv --parallel-workers 4 --ament-cmake-args -DPYTHON_EXECUTABLE=/home/robo01/anaconda3/envs/CondaEnvForRos310/bin/python
# colcon build --symlink-install --packages-select vision_opencv --parallel-workers 4 --ament-cmake-args -DPYTHON_EXECUTABLE=/home/robo01/anaconda3/envs/CondaEnvForRos310/bin/python
#--symlink-install
#--allow-overriding image_geometry

#
# Build - image pipeline from source
# image pipeline
# git clone – b <ros2-distro> git@github.com:ros-perception/image_pipeline.git
cd ${ROS2_WS}
git clone https://github.com/ros-perception/image_pipeline.git
cd image_pipeline
git checkout -b $ROS_DISTRO
git status
cd ${ROS2_WS}
# TODO: set the packages !!! there is more than one !!!
colcon build --packages-select tracetools_image_pipeline image_proc image_publisher image_rotate image_view stereo_image_proc depth_image_proc image_pipeline camera_calibration --symlink-install --allow-overriding image_geometry  vision_opencv --parallel-workers 4 --ament-cmake-args -DPYTHON_EXECUTABLE=/home/robo01/anaconda3/envs/CondaEnvForRos310/bin/python

 ros2 run camera_calibration cameracalibrator --size 7x9 --square 0.02 --ros-args -r image:=/ros_proxy_image -p camera:=/my_camera





