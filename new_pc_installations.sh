#!/bin/bash

# run everything as super user 
# sudo -i 

sudo apt update 

sudo apt-get install -y snapd

# Installation managers that shall - save your computer in case of installation failure 
sudo apt-get install -y synaptic aptitude

# use sudo checkinstall instead of "sudo make install"  for much better package managment process
# https://help.ubuntu.com/community/CheckInstall
# https://stackoverflow.com/questions/1439950/whats-the-opposite-of-make-install-i-e-how-do-you-uninstall-a-library-in-li
sudo apt-get install -y checkinstall

#################### Developer tools start 
# install gcc - used for many development tools 
sudo apt-get -y install gcc g++ gdbserver

# install GCC Compiler
sudo apt install build-essential -y
sudo apt-get install gdb -y

sudo apt install ninja-build -y

# lldb debugger 
sudo apt install lldb -y
sudo apt-get install libtiff-dev -y

# eigen for pcl 
sudo apt install libeigen3-dev -y 

# libtiff for pcl 
sudo apt install libtiff5-dev -y 

# armadillo cpp dependencies 
sudo apt install libopenblas-dev liblapack-dev  libarpack2-dev libsuperlu-dev -y
sudo apt-get install libopenblas-openmp-dev -y 

# armadillo library [1]
sudo apt-get install libblas-dev -y
sudo apt-get install libboost-dev -y

# there is no need to compile armadilo from source 
sudo apt-get install libarmadillo-dev  -y

# opencv dependencies 
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev -y
sudo apt-get install libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev -y
sudo apt-get install libgtk-3-dev libpng-dev libjpeg-dev libopenexr-dev libtiff-dev libwebp-dev -y 

# qt is a base library for many libraries. this is need in order to compile cmake from source 
# https://askubuntu.com/questions/1404263/how-do-you-install-qt-on-ubuntu22-04
sudo apt install -y qtcreator qtbase5-dev qt5-qmake cmake -y

# TODO: install cuda, this failed (dont know why)
# sudo apt install -y nvidia-cuda-toolkit

#################### Developer tools end 

sudo apt install -y net-tools
sudo apt install -y curl

# install ifconfig options 
 sudo apt install -y  net-tools

# screen recorder [1]
sudo apt install -y vokoscreen

# Install google chrome 
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt -y install ./google-chrome-stable_current_amd64.deb
rm google-chrome-stable_current_amd64.deb

############ IDE start 
# install pycharm 
# sudo snap install pycharm-professional --classic
sudo snap install pycharm-community --classic

sudo snap install code --classic

# TODO - install CLION  
############ IDE end 

############ Docker start 
# https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository 

sudo apt-get update
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt install docker.io docker-compose-plugin -y 
sudo apt-get install 
############ Docker end

# install terminator & htop 
sudo add-apt-repository -y ppa:gnome-terminator
sudo apt-get -y install terminator htop

# set terminator as default terminal using [1]

# sudo apt install -y mlocate

# install git 
sudo add-apt-repository ppa:git-core/ppa -y
sudo apt-get update
sudo apt install -y git-all

# Config git with your details 
git config --global user.email  shaharsarshalom@gmail.com
git config --global user.name  shaharsarshalom

sudo snap install gitkraken --classic

# vlc player 
sudo snap install vlc

# install make 
sudo apt-get -y install make cmake-qt-gui

# Developer tools for opencv development in python 
#  → cause error --> sudo add-apt-repository python-opencv

sudo apt-get -y install libopencv-dev

# ַpinta - paint for linux 
sudo snap install pinta

sudo snap install p7zip-desktop

# Download bittorrent client 
sudo apt-get install -y  deluge

# install pdf viewer and editor 
sudo apt install okular -y

# Variety wallpaper changer - optional 
# sudo apt install -y variety
# install variety from ppa, which is newer version 
# https://github.com/varietywalls/variety?tab=readme-ov-file
sudo add-apt-repository -y ppa:variety/stable
sudo apt update
sudo apt install variety
# start variety for first time in order to configure it
# --> run this later:      variety
variety
# solve variety bug in ubuntu 22.04 using this solution https://askubuntu.com/a/1404593/672598


# ubuntu-restricted-extras package allows users to install ability to play popular non-free media formats, including DVD, MP3, Quicktime, and Windows Media formats.
# I’M NOT SURE WE NEED THESE 
#sudo apt-get install -y ubuntu-restricted-extras
#sudo apt-get install -y libavcodec58 ffmpeg
# enable ss h 
#https://stackoverflow.com/questions/17335728/connect-to-host-localhost-port-22-connection-refused
# solve the ssh problem by uninstall and install 
# sudo apt-get remove openssh-client openssh-server
sudo apt-get install -y  openssh-client openssh-server

############ zsh start
# install ohmyzsh -  framework for managing your zsh configuration [1], [2]
sudo apt install zsh -y
# not sure what is this used for
# sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
# make sure installation completed
which zsh
# now make it your default shell
sudo chsh -s /usr/bin/zsh

# install homyzsh on top of zsh !!!
# https://github.com/ohmyzsh/ohmyzsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# install fzf extension
# https://github.com/junegunn/fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

# install zsh-autosuggestions extension
# https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# install zsh-syntax-highlighting extension
# Note that zsh-syntax-highlighting must be the last plugin sourced.
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# argcomplete for ros2 & colcon
eval "$(register-python-argcomplete3 ros2)"
eval "$(register-python-argcomplete3 colcon)"
############ zsh end
